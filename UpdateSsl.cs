// This file is auto-generated, don't edit it. Thanks.

using AlibabaCloud.SDK.FC20230330.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog;
using NLog.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Tea;


namespace AlibabaCloud.SDK.Sample
{
    public class Program
    {
        private static ILogger<Program> logger;

        /**
         * 使用AK&SK初始化账号Client
         * @param accessKeyId
         * @param accessKeySecret
         * @return Client
         * @throws Exception
         */
        public static AlibabaCloud.SDK.FC20230330.Client CreateClient()
        {
            // 工程代码泄露可能会导致 AccessKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考。
            // 建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/378671.html。
            AlibabaCloud.OpenApiClient.Models.Config config = new AlibabaCloud.OpenApiClient.Models.Config
            {
                // 必填，请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_ID。
                AccessKeyId = Environment.GetEnvironmentVariable("ALIBABA_CLOUD_ACCESS_KEY_ID"),
                // 必填，请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_SECRET。
                AccessKeySecret = Environment.GetEnvironmentVariable("ALIBABA_CLOUD_ACCESS_KEY_SECRET"),
            };
            // Endpoint 请参考 https://api.aliyun.com/product/FC
            config.Endpoint = "1864689991159390" + ".cn-hangzhou.fc.aliyuncs.com";
            return new AlibabaCloud.SDK.FC20230330.Client(config);
        }

        public static void Main(string[] args)
        {
            logger = LoggerFactory.Create(builder => builder.AddNLog()).CreateLogger<Program>();
            logger.LogInformation("Program has started.");

            if (args.Count() < 1)
            {
                logger.LogError($"Invalid Arguments: {args.Length}");
                return;
            }

            var hostName = args[0];         // first argument must be host name
            logger.LogInformation($"hostName: {hostName}");

            var startup = new Startup();

            logger.LogInformation($"{startup.AppSettings.SslStoreFolder}");

            var fileName = startup.AppSettings.SslStoreFolder + "\\" + hostName + "-crt.pem";
            var cert = ReadFileContent(fileName);

            fileName = startup.AppSettings.SslStoreFolder + "\\" + hostName + "-key.pem";
            var privateKey = ReadFileContent(fileName);

            var functionName = startup.AppSettings.FunctionName;

            var client = CreateClient();

            AlibabaCloud.SDK.FC20230330.Models.WAFConfig updateCustomDomainInputWAFConfig = new AlibabaCloud.SDK.FC20230330.Models.WAFConfig
            {
                // Boolean, 可选, 是否开启WAF防护
                EnableWAF = false,
            };
            AlibabaCloud.SDK.FC20230330.Models.TLSConfig updateCustomDomainInputTLSConfig = new AlibabaCloud.SDK.FC20230330.Models.TLSConfig
            {
                // Array, 必填, TLS加密套件。
                CipherSuites = new List<string>
                {
                    "TLS_RSA_WITH_RC4_128_SHA"
                },
                // String, 可选, TLS最大版本号。枚举值：TLSv1.3, TLSv1.2, TLSv1.1, TLSv1.0
                MaxVersion = "TLSv1.3",
                // String, 必填, TLS最小版本号。枚举值：TLSv1.3, TLSv1.2, TLSv1.1, TLSv1.0
                MinVersion = "TLSv1.0",
            };
            AlibabaCloud.SDK.FC20230330.Models.PathConfig updateCustomDomainInputRouteConfigPathConfig0 = new AlibabaCloud.SDK.FC20230330.Models.PathConfig
            {
                // Array, 可选, HTTP方法。支持的方法有：HEAD, GET, POST, PUT, DELETE, PATCH, OPTIONS。
                Methods = new List<string>
                {
                    "GET",
                    "POST",
                    "OPTIONS"
                },
                // String, 必填, 函数名
                FunctionName = functionName,
                // String, 必填, 路由匹配规则
                Path = "/*",
                // String, 可选, 版本或者别名
                Qualifier = "LATEST",
            };
            AlibabaCloud.SDK.FC20230330.Models.RouteConfig updateCustomDomainInputRouteConfig = new AlibabaCloud.SDK.FC20230330.Models.RouteConfig
            {
                // Array, 可选
                Routes = new List<AlibabaCloud.SDK.FC20230330.Models.PathConfig>
                {
                    updateCustomDomainInputRouteConfigPathConfig0
                },
            };

            var updateCustomDomainInputCertConfig = new AlibabaCloud.SDK.FC20230330.Models.CertConfig
            {
                // String, 必填, 证书名称
                CertName = hostName + "-" + DateTime.Now.ToString("yyyyMMddhhmmss"),
                // String, 必填, PEM格式证书
                Certificate = cert,
                // String, 必填, PEM格式私钥
                PrivateKey = privateKey,
            };

            AlibabaCloud.SDK.FC20230330.Models.UpdateCustomDomainInput updateCustomDomainInput = new AlibabaCloud.SDK.FC20230330.Models.UpdateCustomDomainInput
            {
                // Object, 可选
                CertConfig = updateCustomDomainInputCertConfig,
                // String, 可选, 域名支持的协议类型。HTTP：仅支持HTTP协议。HTTPS：仅支持HTTPS协议。HTTP,HTTPS：支持HTTP及HTTPS协议。
                Protocol = "HTTP,HTTPS",
                // Object, 可选
                RouteConfig = updateCustomDomainInputRouteConfig,
                // Object, 可选
                TlsConfig = updateCustomDomainInputTLSConfig,
                // Object, 可选
                WafConfig = updateCustomDomainInputWAFConfig,
            };
            AlibabaCloud.SDK.FC20230330.Models.UpdateCustomDomainRequest updateCustomDomainRequest = new AlibabaCloud.SDK.FC20230330.Models.UpdateCustomDomainRequest
            {
                // Object, 可选
                Body = updateCustomDomainInput,
            };

            AlibabaCloud.TeaUtil.Models.RuntimeOptions runtime = new AlibabaCloud.TeaUtil.Models.RuntimeOptions();
            Dictionary<string, string> headers = new Dictionary<string, string>() { };
            try
            {
                UpdateCustomDomainResponse resp = client.UpdateCustomDomainWithOptions(hostName, updateCustomDomainRequest, headers, runtime);
                logger.LogInformation(resp.StatusCode.ToString());

                //if (resp.StatusCode == 200)
                //{
                //    RemoveFiles(startup.AppSettings.SslStoreFolder, hostName);
                //}

                logger.LogInformation($"{JsonConvert.SerializeObject(resp)}");
            }
            catch (TeaException error)
            {
                // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
                // 错误 message
                logger.LogError(error, error.Message);
                //logger.LogError("{error.Data['Recommend']}");
            }
            catch (Exception _error)
            {
                TeaException error = new TeaException(new Dictionary<string, object>
                {
                    { "message", _error.Message }
                });
                // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
                // 错误 message
                logger.LogError(error.Message);
                // 诊断地址
                //logger.LogError("{error.Data['Recommend']}");
            }
        }

        private static string ReadFileContent(string filePath)
        {
            try
            {
                // Read the content of the certificate file
                string content = File.ReadAllText(filePath);

                // Now, 'cert' variable contains the content of your certificate file
                // You can use the 'cert' variable as needed
                logger.LogInformation("File content read successfully.");

                // For demonstration, let's print the first 100 characters (if it's long enough)
                logger.LogInformation(content.Length > 100 ? content.Substring(0, 100) + "..." : content);

                return content;
            }
            catch (Exception ex)
            {
                // If there's an error (e.g., file not found), it will be caught here
                logger.LogError("An error occurred: " + ex.Message);
            }
            return "";
        }

        private static void RemoveFiles(string searchDirectory, string searchString)
        {
            try
            {
                // Get all files in the directory
                string[] files = Directory.GetFiles(searchDirectory);

                // Loop through each file in the directory
                foreach (string file in files)
                {
                    // Check if the file name contains the specified string
                    if (Path.GetFileName(file).Contains(searchString))
                    {
                        // Delete the file
                        File.Delete(file);
                        logger.LogInformation($"Deleted file: {file}");
                    }
                }

                logger.LogInformation("Finished deleting files.");
            }
            catch (Exception ex)
            {
                // If an error occurs, display it
                logger.LogInformation($"An error occurred: {ex.Message}");
            }
        }
    }
}
