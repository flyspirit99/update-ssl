﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlibabaCloud.SDK.Sample
{
    public class Startup
    {
        public Startup()
        {
            var builder = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appSettings.json", optional: false);

            IConfiguration config = builder.Build();

            AppSettings = config.GetSection("AppSettings").Get<AppSettings>();

        }

        public AppSettings AppSettings { get; private set; }
    }
}
