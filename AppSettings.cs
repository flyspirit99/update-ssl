﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlibabaCloud.SDK.Sample
{
    public class AppSettings
    {
        public string SslStoreFolder { get; set; }
        public string FunctionName { get; set; }
    }
}
